package main

import (
	"fmt"

	chapter1 "github.com/krafugo/cracking_interview/chapter1"
)

func main() {
	// fmt.Println(chapter1.IsUnique("Hi! World"))
	fmt.Println(chapter1.CheckPermutation("lona", "pantalones"))
}

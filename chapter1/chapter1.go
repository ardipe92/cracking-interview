// Package chapter1 - Array and Strings
package chapter1

import (
	"fmt"
	"strings"
)

/*
IsUnique implement an algorithm to determine if a string has all unique characters. What if you
cannot use additional data structures?
*/
func IsUnique(str string) bool {
	lower := strings.ToLower(str)
	var mark [256]bool
	for _, char := range lower {
		i := int(char) % len(mark)
		if mark[i] {
			return false
		}
		mark[i] = true
	}
	return true
}

/*
CheckPermutation Given two strings, write a method to decide if one is a permutation of the
other
*/
func CheckPermutation(strA, strB string) bool {
	if len(strA) > len(strB) {
		return false
	}
	permA := calcPermut(strA)
	for i := 0; i < len(strB)-len(strA); i++ {
		sub := string(strB[i : i+len(strA)])
		permB := calcPermut(sub)
		if equalPermut(permA, permB) {
			fmt.Println(sub)
			return true
		}
	}
	return false
}

func calcPermut(str string) map[rune]int {
	permDict := make(map[rune]int)
	for _, c := range str {
		val, ok := permDict[c]
		if ok {
			permDict[c] = val + 1
		}
		permDict[c] = 1
	}
	return permDict
}

func equalPermut(permA, permB map[rune]int) bool {
	if len(permA) != len(permB) {
		return false
	}
	for key, valA := range permA {
		valB, ok := permB[key]
		if !ok && valA != valB {
			return false
		}
	}
	return true
}
